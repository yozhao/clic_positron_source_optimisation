#!/bin/bash

mkdir -p job/Condor/err job/Condor/log job/Condor/out;
mkdir -p job/Arguments job/Root job/Dat job/Results job/Log job/Fig;

## ----------------------------

## Run options

## ----------------------------

option=CLIC380GeV_IPAC2021_OptiAlgorithm;

#option=CLIC380GeV_IPAC2021_AnalyticAMD;
#option=CLIC380GeV_IPAC2021_LinearAMD;
#option=CLIC380GeV_IPAC2021_NonLinearAMD;
#option=CLIC3TeV_IPAC2021_AnalyticAMD;
#option=CLIC3TeV_IPAC2021_LinearAMD;
#option=CLIC3TeV_IPAC2021_NonLinearAMD;

## ----------------------------

## iteration number 
## > 0 for optimisation
## = 0 for final results
## < 0 for tests
index=0;   

## do scan (optimisation) or single run
do_scan=0;

## suffix
[[ $do_scan -eq 0 ]] && prefix="trk_";
[[ $do_scan -eq 1 ]] && prefix="scan_";
[[ $index -le -1 ]] && append="_t"${index/-/};
[[ $index -eq 0 ]] && append="_final";
[[ $index -eq 1 ]] && append="_1st";
[[ $index -eq 2 ]] && append="_2nd";
[[ $index -eq 3 ]] && append="_3rd";
[[ $index -ge 4 ]] && append="_"${index}"th";
suffix=${prefix}${option}${append};

seed=1;

## local run or submit HT Condor jobs
run_local=1;

## tree options: "all", "primary", "photon_emit", "xtal_leave", "amor_arrive", "amor_leave", "amd_arrive","amd_leave"
## use "amor_leave" for optimisation which is faster
tree_option="all";

octave-cli scr/ite_scan.m ${suffix} ${seed} ${run_local} ${tree_option};

