function [nPositrons_eff, pedd, power_deposit, power_beam] = track(P, F, Suffix, I_str, J_str)

  nPositrons_eff = -1;
  pedd = -1;
  power_deposit = -1;
  power_beam = -1;

  global gg;
  global gp;

  addpath([gg.workdir "/scr"]);

  % Add RF-Track installation path on lxplus
  %if( !strcmp(gethostname(), "lxclicbpk20") )
  if( strncmp(gethostname(), "lxplus",6) )
    addpath('/cvmfs/clicbp.cern.ch/x86_64-centos7-gcc8-opt/rf-track-2.1/share/octave/');
  endif

  RF_Track;

  i = 1;
  gp.energy        = P(i++);
  gp.sigmaXY       = P(i++);
  gp.emittance     = P(i++);
  gp.xtal_thick    = P(i++);
  gp.distance      = P(i++);
  gp.mag_field     = P(i++);
  gp.amor_thick    = P(i++);
  gp.amd_fgap      = P(i++);
  gp.B0            = P(i++);
  gp.amd_L         = P(i++);
  gp.amd_R1        = P(i++);
  gp.tw_fgap       = P(i++);
  gp.phi_dec       = P(i++);
  gp.phi_acc       = P(i++);
  gp.grad_dec      = P(i++);
  gp.grad_acc      = P(i++);

  gg.track_Target  = F(1);
  gg.track_AMD     = F(2);
  gg.track_TW      = F(3);
  gg.track_IL      = F(4);

  fullSuffix    = [ Suffix "_" I_str "_" J_str ];
  gg.suffix     = Suffix;
  gg.fullsuffix = fullSuffix;

  gg.TargetOutput = [ "Target_" fullSuffix ];
  gg.AMDOutput    = [ "AMD_"    fullSuffix ];
  gg.TWOutput     = [ "TW_"     fullSuffix ];
  gg.ILOutput     = [ "IL_"     fullSuffix ];

  if( gp.emittance != 0 )
    gamma_e = gp.energy*1e3/RF_Track.electronmass; %% [rad]
    gg.dPxy = gp.emittance / gamma_e / gp.sigmaXY * 1e-3;
  endif
  gg.sigmaE   = gp.energy * 1e3 * gg.dE;   % [MeV]
  gg.sigmaPxy = gp.energy * 1e3 * gg.dPxy; % [MeV]

  gg.size_mesh_xy = gg.amor_width+0.5;
  gg.size_mesh_z  = gp.amor_thick+0.5;
  gg.size_mesh_xy_xtal = gg.xtal_width+0.5;
  gg.size_mesh_z_xtal  = gp.xtal_thick+0.5;
  gg.nbins_mesh_xy = round(gg.size_mesh_xy/gg.mesh_dxy);
  gg.nbins_mesh_z  = round(gg.size_mesh_z/gg.mesh_dz);
  gg.nbins_mesh_xy_xtal = round(gg.size_mesh_xy_xtal/gg.mesh_dxy);
  gg.nbins_mesh_z_xtal  = round(gg.size_mesh_z_xtal/gg.mesh_dz);

  if(gp.amd_fgap+gg.amd_linf_B0ZPos ==0) 
    gg.amd_linf_act = 0;
    printf("INFO:: IsActive status set to 0.\n");
  endif

  %%%%%%%%%%%%%%%%%%%%%%%%%%% Target tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if(gg.track_Target) 

    printf("INFO:: Beam parameters:\n");
    printf("       Primary electron energy: %f [GeV]\n", gp.energy);
    printf("       Primary electron beam spot-size: %f [mm]\n", gp.sigmaXY);
    printf("       Primary electron beam emittance: %f [mm.mrad]\n", gp.emittance);
    printf("       Primary electron beam E spread: %f [MeV]\n", gg.sigmaE);
    printf("       Primary electron beam pT spread: %f [MeV]\n", gg.sigmaPxy);

    printf("INFO:: Target input parameters:\n");
    printf("       Crystal thickness: %f [mm]\n", gp.xtal_thick);
    printf("       Distance: %f [m]\n", gp.distance);
    printf("       Magnetic field: (0, %f, 0) [T]\n", gp.mag_field);
    printf("       Amorphous thickness: %f [mm]\n", gp.amor_thick);

    printf("       mesh_dxy: %f [mm]\n", gg.mesh_dxy);
    printf("       mesh_dz: %f [mm]\n", gg.mesh_dz);

    printf("INFO:: AMD linear fringe field parameters:\n");
    printf("       IsActive status: %i \n", gg.amd_linf_act);
    printf("       B0: %f [T]\n", gp.B0);
    printf("       K factor: %f [T/mm]\n", gg.amd_linf_K);
    printf("       B0 position (wrt AMD): %f [mm]\n", gg.amd_linf_B0ZPos);
    printf("       Target position (wrt AMD): %f [mm]\n", -gp.amd_fgap);

    %% prepare geant4 input file
    targetConfigFile = [gg.workdir "/job/Dat/" gg.TargetOutput ".mac"];
    prepare_G4(targetConfigFile);
    
    %% run geant4
    num_cores = RF_Track.number_of_threads;
    printf("INFO:: Number of CPU cores used: %i\n",num_cores);
    target_bin = [gg.workdir "/../G4/Injector_build"];
    tic
      system([target_bin "/injector " targetConfigFile " " num2str(num_cores) " " gg.tree_option " " gg.seed]);
    toc
 
    %% merge root files
    if length(glob([ gg.workdir "/job/Root/" gg.TargetOutput "_t*" ])) > 0
        system(["hadd -f " gg.workdir "/job/Root/" gg.TargetOutput ".root " gg.workdir "/job/Root/" gg.TargetOutput "_t*.root"]);
        system(["rm -f " gg.workdir "/job/Root/" gg.TargetOutput "_t*.root"]);
    endif

    %% convert root file into .dat file for only positrons
    system(["root -l -b -q '" gg.workdir "/scr/convert_ROOT_To_RFTrack.C(\"" gg.TargetOutput "\",\"" gg.workdir "\")'"]);

    %% calculate nPositrons, in-situ pedd
    try
      A_target = load([gg.workdir "/job/Dat/" gg.TargetOutput ".dat"]);
    catch
      printf("WARNING__:: Empty file: job/Dat/%s.dat.\n", gg.TargetOutput);
      return;
    end_try_catch

    nPositrons = size(A_target)(1);
    printf("INFO:: After Target:\n");
    printf("       nPositrons: %i\n",nPositrons);
    yield = nPositrons * 1.0 / gg.Ne;
    printf("       yield: %f\n",yield);

    [pedd_insitu, power_deposit_insitu] = calc_pedd(nPositrons);
    power_beam_insitu = calc_power(nPositrons);

    filename = [gg.workdir "/job/Dat/" gg.TargetOutput ".dat"];
    save("-text",filename,"nPositrons","yield","pedd_insitu","power_deposit_insitu","power_beam_insitu","P","F","gp","gg","A_target");

  else
    disp("INFO:: Tracking of target skipped.")
  endif
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Target tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %%%%%%%%%%%%%%%%%%%%%%%%%%% AMD tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if(gg.track_AMD)

    try
        A_target = load([gg.workdir "/job/Dat/" gg.TargetOutput ".dat"]).A_target;
    catch
        printf("WARNING:: Empty file: job/Dat/%s.dat.\n", gg.TargetOutput);
        return;
    end_try_catch
        
    Bunch.mass = RF_Track.electronmass; % MeV/c/c
    Bunch.charge = +1; %% units of e+
    B_target_6d = Bunch6d(Bunch.mass, ...
                       0, ...
                       Bunch.charge, ...
                       A_target);

    %% Track with Volume
    V = Volume();
    V.set_aperture(1, 1, 'circular'); %% [m]

    %% AMD options

    if(0)

    elseif(strcmp(gg.amd_option,'AnalyticFCLike'))

      Bl = gg.sol_bz; %% [T]
      Mu = ((gp.B0/Bl) - 1) / (gp.amd_L*1e-2-gg.amd_linf_B0ZPos*1e-3);  %% [1/m]
      AMDR2 = gp.amd_R1*sqrt(gp.B0/Bl);%% mm

      %%AMDR1 = 20*sqrt(gg.sol_bz/gp.B0);%% mm
      %%AMDR2 = 20; %% mm

      printf("INFO:: AMD linear fringe field parameters:\n");
      printf("       IsActive status: %i \n", gg.amd_linf_act);
      printf("       B0: %f [T]\n", gp.B0);
      printf("       K factor: %f [T/mm]\n", gg.amd_linf_K);
      printf("       B0 position (wrt AMD): %f [mm]\n", gg.amd_linf_B0ZPos);
      printf("       Target position (wrt AMD): %f [mm]\n", -gp.amd_fgap);

      printf("INFO:: AMD analytic field parameters:\n");
      printf("       B0: %f T\n", gp.B0);
      printf("       Length: %f cm\n",   gp.amd_L);
      printf("       Mu: %f m^-1\n",     Mu);
      printf("       Apertures: [%f, %f] mm\n", gp.amd_R1, AMDR2);
    
      %% AMD geometry
      AMD_SHAPE = AdiabaticMatchingDevice( gp.amd_L*1e-2, ... %% [m], L
                                           0, ...  %% [T], B0
                                           0);     %% [1/m], mu
      AMD_SHAPE.set_nsteps(1000);
      AMD_SHAPE.set_odeint_algorithm("rkf45");
      AMD_SHAPE.set_entrance_aperture(gp.amd_R1*1e-3); %% [m]
      AMD_SHAPE.set_exit_aperture(AMDR2*1e-3);
      AMD_SHAPE.set_static_Bfield(0,0,0);

      %% AMD linear fringe field
      if(gg.amd_linf_act)
        nstep_LINF = 100;
        step_LINF = 1.0*(gp.amd_fgap+gg.amd_linf_B0ZPos) / nstep_LINF; %% [mm]
        for i = 1:nstep_LINF
          bz = gp.B0 - gg.amd_linf_K*(gp.amd_fgap+gg.amd_linf_B0ZPos-(i-1)*step_LINF);
          Bz_LINF(i) = bz; %% [T]
        endfor
        AMD_LINF = Static_Magnetic_FieldMap_1d(Bz_LINF, step_LINF*1e-3, -1);
        AMD_LINF.set_nsteps(nstep_LINF);
        AMD_LINF.set_odeint_algorithm("rk2");
        AMD_LINF.set_aperture(1, 1, "circular"); %% meter
      endif

      %% AMD analytic field
      AMD_ANALYTIC = AdiabaticMatchingDevice( gp.amd_L*1e-2 - gg.amd_linf_B0ZPos*1e-3, ... %% [m], L
                                              gp.B0, ...  %% [T], B0
                                              Mu);     %% [1/m], mu
      AMD_ANALYTIC.set_nsteps(1000);
      AMD_ANALYTIC.set_odeint_algorithm("rk2");
      AMD_ANALYTIC.set_entrance_aperture(1); %% [m]
      AMD_ANALYTIC.set_exit_aperture(1);
      AMD_ANALYTIC.set_static_Bfield(0,0,0);

      V.add(AMD_SHAPE, 0, 0, gp.amd_fgap*1e-3, 'entrance'); %% [m]
      if(gg.amd_linf_act) V.add(AMD_LINF, 0, 0, 0, 'entrance'); endif %% [m]
      V.add(AMD_ANALYTIC, 0, 0, (gp.amd_fgap+gg.amd_linf_B0ZPos)*1e-3, 'entrance'); %% [m]

      amd.L_total = gp.amd_fgap + gp.amd_L*1e1; %% [mm]

    elseif( strcmp(gg.amd_option,'LinearFC') || strcmp(gg.amd_option,'NonLinearFC') )

      if (strcmp(gg.amd_option,'LinearFC'))

        design_option = 'Lin_Type1_R6.5_I13.8_F25';

      elseif (strcmp(gg.amd_option,'NonLinearFC'))

        design_option = 'Inv_Type2_R6.5_I13.8_F25';

      endif

      amd.field_file = [ gg.workdir '/field/Hugo/Processed_Shaped_' design_option '.dat' ];

      load(amd.field_file);

      amd.field.dz = Z(1,2)-Z(1,1); %% [mm]
      amd.field.dr = R(2,1)-R(1,1); %% [mm]

      amd.R2 = Rimax_AMD; %% [mm], Inv: 43.22, Lin:55.45
      %amd.L = L_AMD; %% [mm] AMD length, 127.02

      %% scale field by peak (B0)
      amd.peak_bz = max(Bz(1,:));
      if (gg.amd_scaling)
        amd.sf = 1.0 * gp.B0 / amd.peak_bz;
      else
        amd.sf = 1.0;
      endif
      Br = Br * amd.sf;
      Bz = Bz * amd.sf;
      printf("INFO:: Scaling factor %.2f applied to AMD field with peak %.2f T.\n", amd.sf, amd.peak_bz);

      %% re-calculate amd.L (including field extension length)
      M_Z = Z(1,:) > 5; %% [mm]
      iz1 = lookup(Bz(1,M_Z), gg.sol_bz);
      iz1 = iz1 + columns(Z(1,:)) - columns(Z(1,M_Z));
      amd.L = Z(1,iz1);

      iz0 = lookup(Z(1,:), -gp.amd_fgap);
      if (iz0==0) 
        iz0 = 1; 
	gp.amd_fgap = 0 - Z(1,1);
	printf("WARNING:: AMD front gap forced to be %f mm, due to limited field map length.\n",gp.amd_fgap);
      endif
      if (iz1==0) 
        iz1 = columns(Z);
      endif
      Br = Br(:,iz0:iz1);
      Bz = Bz(:,iz0:iz1);

      amd.L_total = gp.amd_fgap + amd.L; %% [mm]
      
      AMD = Static_Magnetic_FieldMap_2d(Br', Bz', amd.field.dr*1e-3, amd.field.dz*1e-3); %% [T, m]
      AMD.set_length(amd.L_total*1e-3); %% [m]
      AMD.set_aperture(amd.R2*1e-3, amd.R2*1e-3, "circular"); %% [m], constant R
      AMD.set_odeint_algorithm("rkf45");

      V.add(AMD, 0, 0, 0, 'entrance'); %% [m]

      gp.amd_L = amd.L*1e-1; %% [cm], reset gp.amd_L

    elseif(strncmp(gg.amd_option,'PSIHTS_',7))

      disp('INFO:: tracking PSI HTS AMD . . .');
      design_option = strrep(gg.amd_option,'PSIHTS_','');
      amd.fieldmap_file = ['field/PSIHTS/Processed_' design_option '.dat'];
      amd.Ztc = -gp.amd_fgap; % Target exit to peak field
      if(strcmp(design_option,'Apr2022'))
        amd.half_length = 96.5;
      endif
      amd.Bc = gg.sol_bz;
      amd.Ri = gp.amd_R1;
  
      zv.target_exit = 0; % target exit position in Volume in mm, always being 0

      %% Load AMD field map

      amd.field = load(amd.fieldmap_file);
      
      amd.field.dz = amd.field.Z(1,2)-amd.field.Z(1,1); %% [mm]
      amd.field.dr = amd.field.R(2,1)-amd.field.R(1,1); %% [mm]

      amd.Z_axis  = amd.field.Z(1,:); % on-axis Z
      amd.Bz_axis = amd.field.Bz(1,:); % on-axis Bz

      % Get effective field and length used in tracking, starts from target exit, stops at constant solenoid field value

      amd.zfte = amd.Ztc; % target exit position in field map in mm

      izte = lookup(amd.Z_axis, amd.zfte);
      izbc = length(amd.Z_axis) - lookup(flip(amd.Bz_axis),amd.Bc) + 1;

      amd.field.Br_eff = amd.field.Br(:, izte:izbc);
      amd.field.Bz_eff = amd.field.Bz(:, izte:izbc);
      amd.Z_axis_eff  = amd.Z_axis(izte:izbc);
      amd.Bz_axis_eff = amd.Bz_axis(izte:izbc);

      amd.field_length = range(amd.Z_axis_eff); 

      %% AMD field

      HTS_FIELD = Static_Magnetic_FieldMap_2d(amd.field.Br_eff', amd.field.Bz_eff', amd.field.dr*1e-3, amd.field.dz*1e-3); %% [T, m]
      HTS_FIELD.set_length(amd.field_length*1e-3); %% [m]
      %HTS_FIELD.set_nsteps(floor(amd.field_length*10));
      HTS_FIELD.set_nsteps(floor(amd.field_length*2));
      HTS_FIELD.set_odeint_algorithm("rkf45");

      %% HTS solenoid shape
      
      HTS_SHAPE = AdiabaticMatchingDevice( amd.half_length*2*1e-3, 0, 0); % [L, B0, Mu] in [m, T, 1/m]
      HTS_SHAPE.set_nsteps(200);
      HTS_SHAPE.set_odeint_algorithm("rk2");
      HTS_SHAPE.set_entrance_aperture(amd.Ri*1e-3); % m
      HTS_SHAPE.set_exit_aperture(amd.Ri*1e-3); % m
      HTS_SHAPE.set_static_Bfield(0,0,0); % T

      zv.HTS_exit = zv.target_exit - amd.Ztc + amd.half_length; % HTS exit position in Volume in mm

      V.add(HTS_SHAPE, 0, 0, zv.HTS_exit*1e-3, 'exit'); % m
      V.add(HTS_FIELD, 0, 0, zv.target_exit*1e-3, 'entrance'); % m

      gp.amd_L = amd.field_length*1e-1; %% [cm], reset gp.amd_L
      amd.L_total = gp.amd_fgap + gp.amd_L*1e1; %% [mm]

    endif

    V.set_s0(0); %% [m]
    V.set_s1(amd.L_total*1e-3); %% [m]
    T = TrackingOptions();
      %T.dt_mm = 0.1; %% [mm/c]
      T.dt_mm = 1; %% [mm/c]
      T.t_max_mm = Inf;
      T.backtrack_at_entrance = false; %% start tracking at s0
      T.odeint_algorithm = "rkf45"; %% 'rk2', 'rkf45', 'rk8pd'
      T.odeint_epsabs = 1e-5;
      %T.open_boundaries = true; % deleted by Andrea on 10 Aug 2022

    %% Get on-axis bz 
    Bz_plot = [];
    z0 = -gp.amd_fgap; %% [mm]
    z1 = z0 + amd.L_total; %% [mm]
    nz = floor((z1-z0)/0.1+1);
    Z_plot = linspace(z0, z1, nz); % mm
    for z = Z_plot
      [E_plot,B_plot] = V.get_field(0, 0, z-z0, 0);
      Bz_plot = [ Bz_plot ; B_plot(3) ];
    end

    %% Draw Bz
    if(0)
      plot(Z_plot, Bz_plot);
      xlim([z0 z1]);
      ylim([0 ceil(max(Bz_plot))+1]);
      pause();
    endif

    disp("INFO:: Tracking AMD with Volume...")
    tic;
      B_target_6dT = Bunch6dT(B_target_6d);
      %disp('DEBUG:: Bunch6dT() done.');
      B_amd_6dT = V.track(B_target_6dT,T);
      %disp('DEBUG:: V.track() done.');
      B_amd_6d = V.get_bunch_at_s1();
      %disp('DEBUG:: V.get_bunch_at_s1() done.');
    toc;
    
    A_AMD = B_amd_6d.get_phase_space("%x %xp %y %yp %t %Pc");
    %disp('DEBUG:: B_amd_6d.get_phase_space() done.');

    %% calculate nPositrons, in-situ pedd

    nPositrons = size(A_AMD)(1);
    printf("INFO:: After AMD:\n");
    printf("       nPositrons: %i\n",nPositrons);
    eff = nPositrons * 1.0 / size(A_target)(1);
    printf("       Collection efficiency: %.3f\n",eff);
    yield = nPositrons * 1.0 / gg.Ne;
    printf("       yield: %.2f\n",yield);

    filename = [gg.workdir "/job/Dat/" gg.AMDOutput ".dat"];
    if( !gg.opt_running ) 
      %% Lost particles
      A_LOSS = B_amd_6dT.get_lost_particles();
      save("-text",filename,"nPositrons","yield","P","F","gp","gg","A_AMD","A_LOSS","Z_plot","Bz_plot");
    else
      save("-text",filename,"nPositrons","yield","P","F","gp","gg","A_AMD","Z_plot","Bz_plot");
    endif
  else
    disp("INFO:: Tracking of AMD skipped.")
  endif 

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of MD tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  %%%%%%%%%%%%%%%%%%%%%%%%%%% TW tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if(gg.track_TW) 

    %if( !strcmp(gg.amd_option,"LinearAndAdiabatic") && gp.amd_L==0 )
    if( gp.amd_L==0 )
      %printf("WARNING:: Hugo AMD used. Length is 0. Please track also AMD to reset it automatically.\n");
      printf("WARNING:: AMD length is 0. Please track also AMD to reset it automatically.\n");
    endif

    printf("INFO:: TW input parameters:\n");
    printf("       Front gap: %f mm\n", gp.tw_fgap);
    printf("       Decelerating phase: %f degree\n", gp.phi_dec);
    printf("       Accelerating phase: %f degree\n", gp.phi_acc);
    printf("       Decelerating gradient: %f MV/m\n", gp.grad_dec);
    printf("       Accelerating gradient: %f MV/m\n", gp.grad_acc);

    printf("INFO:: Loading field/field_EB_TW1_cst.dat.gz ...\n");
    load([gg.workdir "/field/field_EB_TW1_cst.dat.gz"])

    %nsteps_default = 1000;
    nsteps_default = 50;
    
    TW_FGAP = Drift(gp.tw_fgap*1e-3);
    TW_FGAP.set_static_Bfield(0,0,gg.sol_bz);
    TW_FGAP.set_odeint_algorithm("rk2");
    TW_FGAP.set_nsteps(nsteps_default);
    TW_FGAP.set_aperture(1, 1, "circular"); %% meter
    
    TW = RF_FieldMap(  M.Ex, ...  %% electric field, V/m
                       M.Ey, ...
                       M.Ez, ...
                       M.Bx, ...  %% magnetic field, T
                       M.By, ...
                       M.Bz, ...
                       M.ra(1), ...
                       M.ta(1), ...
                       M.ra(2) - M.ra(1), ...
                       M.ta(2) - M.ta(1), ...
                       M.za(2) - M.za(1), ...
                       M.za(end), ...
                       gg.f_rf, 1);
    
    TW.set_static_Bfield(0,0,gg.sol_bz); %% tesla
    TW.set_cylindrical(true);
    TW.set_nsteps(nsteps_default);
    TW.set_odeint_algorithm("rk2");
    TW.set_aperture(0.02, 0.02, "circular"); %% meter
    
    DR = Drift(0.2);
    DR.set_static_Bfield(0,0,gg.sol_bz);
    DR.set_odeint_algorithm("rk2");
    DR.set_nsteps(nsteps_default);
    DR.set_aperture(0.02, 0.02, "circular"); %% meter
    
    N_TW = 11;
    TIME_TW        = 150;    %% = (3 cells length) mm/c
    TIME_TW_And_DR = 1700;   %% = (1.5m length + 0.2m distance) mm/c
    TIME_Start_DEC = gp.xtal_thick + gp.distance*1e3 + gp.amor_thick + gp.amd_fgap + gp.amd_L*1e1 + gp.tw_fgap; %% mm/c

    printf("INFO:: NC solenoid field = %.2f T\n",gg.sol_bz);
    printf("INFO:: Number of TW structures = %i\n",N_TW);
    %printf("INFO:: Length of each [TW structure, drift distance] = [1.5, 0.2] m\n");
    printf("INFO:: Length of each TW structure = 1.5 m\n");
    printf("INFO:: Length of TW structure distance 1 = %.2f m\n", gg.tw_dr1);
    printf("INFO:: Length of TW structure distances 2-11 = %.2f m\n", gg.tw_dr2);
    printf("INFO:: TIME_Start_DEC = %f [mm/c]\n",TIME_Start_DEC);

    %% get input bunch
    Bunch.mass       = RF_Track.electronmass; %% MeV/c/c
    Bunch.charge     = +1;  %% units of e+
    try
        A_AMD = load([gg.workdir "/job/Dat/" gg.AMDOutput ".dat"]).A_AMD;
    catch
        printf("WARNING:: Empty file: job/Dat/%s.dat.\n", gg.AMDOutput);
        return;
    end_try_catch
    printf("INFO:: Input of TW:\n");
    printf("       nPositrons: %i\n", size(A_AMD)(1));
    B_amd = Bunch6d(Bunch.mass, ...
                    0, ...
                    Bunch.charge, ...
                    A_AMD);

    if( !gg.opt_running ) 
      L_TW1 = Lattice(); 
      L_TW1.append(TW_FGAP);
    endif
    L_TW = Lattice();
    L_TW.append(TW_FGAP);

    t0 = TIME_Start_DEC;
    for loop_tw = 1:N_TW
        if loop_tw == 1
            TW.set_phid(gp.phi_dec);
            TW.set_P_actual((gp.grad_dec/11.23)**2);
	    DR.set_length(gg.tw_dr1);
        else
            TW.set_phid(gp.phi_acc);
            TW.set_P_actual((gp.grad_acc/11.23)**2);
	    DR.set_length(gg.tw_dr2);
        endif
        
        for loop = 1:10
	    %% t0 set to reference particle travelling in speed of light
            %TW.set_t0(TIME_Start_DEC + (loop_tw-1)*TIME_TW_And_DR + (loop-1) * TIME_TW);
            TW.set_t0(t0);
            if(!gg.opt_running && loop_tw==1) L_TW1.append(TW); endif;
            L_TW.append(TW);
	    t0 += TW.get_length()*1e3; %% [mm/c]
        endfor
        
        if loop_tw != N_TW
            L_TW.append(DR);
	    t0 += DR.get_length()*1e3; %% [mm/c]
        endif
    endfor
    
    fprintf("INFO:: Tracking TW (N=%i) ...\n",N_TW);
    tic
      if( !gg.opt_running ) B_TW1 = L_TW1.track(B_amd); endif
      B_TW = L_TW.track(B_amd);
    toc
    
    if( !gg.opt_running ) A_TW1 = B_TW1.get_phase_space("%x %xp %y %yp %t %Pc"); 
    else A_TW1 = []; endif
    A_TW = B_TW.get_phase_space("%x %xp %y %yp %t %Pc");

    %% calculate nPositrons, in-situ pedd

    if( !gg.opt_running )
      nPositrons_TW1 = size(A_TW1)(1);
      printf("INFO:: After   TW1:\n");
      printf("       nPositrons_TW1: %i\n",nPositrons_TW1);
    endif

    nPositrons = size(A_TW)(1);
    printf("INFO:: After   TW:\n");
    printf("       nPositrons: %i\n",nPositrons);

    yield = nPositrons * 1.0 / gg.Ne;
    printf("       yield: %f\n",yield);

    %% transport table (e_mean evolution)
    if( !gg.opt_running ) A_TT = L_TW.get_transport_table("%S %mean_E %sigma_t %sigma_E");
    else A_TT = []; endif

    filename = [gg.workdir "/job/Dat/" gg.TWOutput ".dat"];
    save("-text",filename,"nPositrons","yield","P","F","gp","gg","A_TW1","A_TW","A_TT");
  else
    disp("INFO:: Tracking of TW skipped.")
  endif 
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of TW tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %%%%%%%%%%%%%%%%%%%%%%%%%%% IL tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if(gg.track_IL) 
    try
        A_TW = load([gg.workdir "/job/Dat/" gg.TWOutput ".dat"]).A_TW;
    catch
        printf("WARNING:: Empty file: job/Dat/%s.dat.\n", gg.TWOutput);
        return;
    end_try_catch

    window_e1 = gg.energy_IL*(1-gg.sigmaEp);
    window_e2 = gg.energy_IL*(1+gg.sigmaEp);

    %% optimise ref_t, ref_e, e_def
    printf("INFO:: Optimising ref_t for IL ...\n");
    t_min = floor(min(A_TW(:,5)));
    t_max = ceil(max(A_TW(:,5)));
    np_eff_opt = 0;
    for t_ref = t_min : 1 : t_max
      window_t1 = t_ref - gg.delta_t*0.5;
      window_t2 = t_ref + gg.delta_t*0.5;
      M_cutT = A_TW(:,5) >= window_t1 & A_TW(:,5) <= window_t2;
      A_TW_cutT = A_TW(M_cutT,:);
      if (rows(A_TW_cutT(:,6))==0) continue; endif
      e_ref = mean(A_TW_cutT(:,6)); 
      %% calculate energy deficit
      e_def = 0;
      for i_fix = 1:50
        A_IL_cutT_def = A_TW_cutT;
        A_IL_cutT_def(:,6) = A_TW_cutT(:,6)  + (gg.energy_IL - e_ref + e_def) ...
                             * cos(2 * pi * gg.f_rf  * (A_TW_cutT(:,5) - t_ref) * 1e-3 / RF_Track.clight);
        M_E_def = A_IL_cutT_def(:,6) >= window_e1 & A_IL_cutT_def(:,6) <= window_e2;
	if (rows(A_IL_cutT_def(M_E_def,6))==0) e_def = e_def + gg.energy_IL;
        else e_def = e_def + gg.energy_IL - mean(A_IL_cutT_def(M_E_def,6));
	endif
      endfor
      %% energy calculation with deficit fixed
      A_IL_cutT = A_TW_cutT;
      A_IL_cutT(:,6) = A_TW_cutT(:,6)  + (gg.energy_IL - e_ref + e_def) ...
                       * cos(2 * pi * gg.f_rf  * (A_TW_cutT(:,5) - t_ref) * 1e-3 / RF_Track.clight);
      M_E = A_IL_cutT(:,6) >= window_e1 & A_IL_cutT(:,6) <= window_e2;
      A_IL_cutTE = A_IL_cutT(M_E,:);
      np_eff = rows(A_IL_cutTE);
      if (np_eff>np_eff_opt)
        np_eff_opt = np_eff;
        gg.ref_t = t_ref;
	gg.ref_e = e_ref;
	gg.e_def = e_def;
      endif
    endfor

    printf("INFO:: Reference particle (optimised) for IL:\n");
    printf("       reference time  : %.1f mm/c\n", gg.ref_t);
    printf("       reference energy: %.1f MeV\n",  gg.ref_e);
    printf("INFO:: Energy deficit for IL: %.1f MeV\n", gg.e_def);

    %% energy calculation for IL
    window_t1 = gg.ref_t - gg.delta_t*0.5;
    window_t2 = gg.ref_t + gg.delta_t*0.5;
    A_IL = A_TW;
    A_IL(:,6) = A_TW(:,6)  + (gg.energy_IL - gg.ref_e + gg.e_def) ...
                * cos(2 * pi * gg.f_rf  * (A_TW(:,5) - gg.ref_t) * 1e-3 / RF_Track.clight);
    M_cutT = A_IL(:,5) >= window_t1 & A_IL(:,5) <= window_t2;
    M_cutE = A_IL(:,6) >= window_e1 & A_IL(:,6) <= window_e2;
    M_cutTE = M_cutT & M_cutE;
    A_IL_cutT = A_IL(M_cutT,:);
    A_IL_cutE = A_IL(M_cutE,:);
    A_IL_eff = A_IL(M_cutTE,:);
    
    % calculate nPositrons, in-situ pedd and nPositrons_eff, pedd
    
    nPositrons = rows(A_IL);
    nPositrons_cutT = rows(A_IL_cutT);
    nPositrons_cutE = rows(A_IL_cutE);
    nPositrons_eff = rows(A_IL_eff);
    printf("INFO:: After IL:\n");
    printf("       MeanE: %.3f [GeV]\n", mean(A_IL(:,6))/1e3);
    printf("       MeanE_cutT: %.3f [GeV]\n", mean(A_IL_cutT(:,6))/1e3);
    printf("       MeanE_cutE: %.3f [GeV]\n", mean(A_IL_cutE(:,6))/1e3);
    printf("       MeanE_eff: %.3f [GeV]\n", mean(A_IL_eff(:,6))/1e3);
    printf("       nPositrons: %i\n", nPositrons);
    printf("       nPositrons_cutT: %i\n", nPositrons_cutT);
    printf("       nPositrons_cutE: %i\n", nPositrons_cutE);
    printf("       nPositrons_eff: %i\n",nPositrons_eff);

    yield_eff = nPositrons_eff * 1.0 / gg.Ne;
    printf("       yield_eff: %f\n",yield_eff);

    [pedd, power_deposit] = calc_pedd(nPositrons_eff);
    power_beam = calc_power(nPositrons_eff);
    printf("INFO:: Normalised results:\n");
    printf("       pedd: %f J/g\n", pedd);
    printf("       power_deposit: %f kW\n", power_deposit);
    printf("       power_beam: %f kW\n", power_beam);

    filename = [gg.workdir "/job/Dat/" gg.ILOutput ".dat"];
    save("-text",filename,"nPositrons","nPositrons_eff","yield_eff","pedd","power_deposit","power_beam","A_IL","A_IL_cutT","A_IL_cutE","A_IL_eff","window_e1","window_e2","window_t1","window_t2");
  else
    disp("INFO:: Tracking of IL skipped.")
  endif %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of IL tracking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

endfunction
