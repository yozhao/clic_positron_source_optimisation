#!/bin/bash
  
#option=CLIC380GeV_Nov2020
#option=CLIC3TeV_Nov2020
option=CLIC380GeV_Nov2020_HYBRID;
#option=CLIC3TeV_Nov2020_HYBRID;
#option=CLIC380GeV_Nov2020_HugoAMD;
#option=CLIC3TeV_Nov2020_HugoAMD;
#option=CLIC380GeV_Nov2020_HugoLinAMD;
#option=CLIC3TeV_Nov2020_HugoLinAMD;

#index=1st
#index=final
index=1001th

root -l -b -q scr/draw_scan.C\(\"$option\",\"$index\"\)

#cp job/Fig/scan_${option}_${index}_*.pdf ~/ws/www/TMP/
