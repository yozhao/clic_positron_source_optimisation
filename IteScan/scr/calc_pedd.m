function [pedd, power] = calc_pedd(nPositrons)

  global gg;
  global gp;

  %% (in-situ) yield

  yield = nPositrons * 1.0 / gg.Ne;

  %% parameters

  Ne_s = gg.Ne; %% number of simulated e-
  Nb_e = gg.Nb; %% number of e- bunches per pulse
  Np_b = gg.Np; %% target e+ bunch charge
  Ne_b = Np_b / yield; %% e- bunch charge
  f_rep = gg.fb; %% Hz, pulse frequency

  density = 19.25; 	%% g/cm^3
  factor_GeV_to_J = 1.60218e-10;
  factor_GeV_to_kW = factor_GeV_to_J*1.0e-3; %% per second

  printf("INFO:: PEDD calculation parameters:\n");
  printf("       mesh_dxy: %f [mm]\n", gg.mesh_dxy);
  printf("       mesh_dz: %f [mm]\n", gg.mesh_dz);
  printf("       size_mesh_xy: %f [mm]\n", gg.size_mesh_xy);
  printf("       size_mesh_z: %f [mm]\n", gg.size_mesh_z);
  printf("       nbins_mesh_xy: %f [mm]\n", gg.nbins_mesh_xy);
  printf("       nbins_mesh_z: %f [mm]\n", gg.nbins_mesh_z);

  %% mesh size

  NBINS_X = gg.nbins_mesh_xy;
  NBINS_Y = gg.nbins_mesh_xy;
  NBINS_Z = gg.nbins_mesh_z;

  delta_x = gg.mesh_dxy; %% mm
  delta_y = gg.mesh_dxy;
  delta_z = gg.mesh_dz;

  volume = delta_x * delta_y * delta_z * 1.e-3; 	%% cm^3

  %% dat file

  xtal_file = [gg.workdir "/job/Dat/" gg.TargetOutput "_xtal.dat"]; 
  amor_file = [gg.workdir "/job/Dat/" gg.TargetOutput "_amorph.dat"]; 

  %% not necessary for now
  %if (exist(xtal_file,"file")!=2)
  %  TrkTargetOutput = [ "Target_trk_" gg.suffix "_0_0" ];
  %  xtal_file = [gg.workdir "/job/Dat/" TrkTargetOutput "_xtal.dat"]; 
  %endif
  %if (exist(amor_file,"file")!=2)
  %  TrkTargetOutput = [ "Target_trk_" gg.suffix "_0_0" ];
  %  amor_file = [gg.workdir "/job/Dat/" TrkTargetOutput "_amorph.dat"]; 
  %endif

  try
    A_xtal = load(xtal_file);
  catch
    A_xtal = [0 0 0 0];
  end_try_catch

  try
    A_amor = load(amor_file);
  catch
    A_amor = [0 0 0 0];
  end_try_catch

  %% calculation

  peak_energy_xtal = max(A_xtal(:,4)) * 1e-3; %% GeV
  peak_energy_amor = max(A_amor(:,4)) * 1e-3; %% GeV
  total_energy_xtal = sum(A_xtal(:,4)) * 1e-3; %% GeV
  total_energy_amor = sum(A_amor(:,4)) * 1e-3; %% GeV

  pedd_xtal = peak_energy_xtal * (Nb_e*Ne_b/Ne_s) / (volume*density) * factor_GeV_to_J;
  pedd_amor = peak_energy_amor * (Nb_e*Ne_b/Ne_s) / (volume*density) * factor_GeV_to_J;
  power_xtal = total_energy_xtal * (f_rep*Nb_e*Ne_b/Ne_s) * factor_GeV_to_kW;
  power_amor = total_energy_amor * (f_rep*Nb_e*Ne_b/Ne_s) * factor_GeV_to_kW;

  %% (in-situ) PEDD, Power, etc.

  printf("INFO:: given yield = %f, PEDD (xtal) = %f J/g\n", yield, pedd_xtal);
  printf("INFO:: given yield = %f, PEDD (amor) = %f J/g\n", yield, pedd_amor);
  printf("INFO:: given yield = %f, Deposited power (xtal) = %f kW\n", yield, power_xtal);
  printf("INFO:: given yield = %f, Deposited power (amor) = %f kW\n", yield, power_amor);

  %%% given yield=1
  %printf("INFO:: given yield = 1, PEDD (amor) = %f J/g\n", (pedd_amor*yield/1.0) );

  %%% safe yield %%
  %min_yield = pedd_amor*yield/35.0;
  %printf("INFO:: safe minimum yield: (pedd_amor*yield/35) = %f\n",min_yield);

  %% return values

  pedd = pedd_amor;
  power = power_amor;

endfunction
