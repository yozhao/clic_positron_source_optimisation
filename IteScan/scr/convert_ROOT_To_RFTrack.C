void convert_ROOT_To_RFTrack(const char *fileName = "output_geant4", const char* workdir = "."){

  TFile * fileROOT  = new TFile(TString(workdir)+"/job/Root/"+TString(fileName) + ".root", "READ");

  TTree *tree = (TTree*)fileROOT->Get("amor_leave");

  Int_t pdgId;
  Double_t x, y, px, py, pz, t;

  tree->SetBranchAddress("pdgId",&pdgId);
  tree->SetBranchAddress("x",&x);
  tree->SetBranchAddress("y",&y);
  tree->SetBranchAddress("px",&px);
  tree->SetBranchAddress("py",&py);
  tree->SetBranchAddress("pz",&pz);
  tree->SetBranchAddress("t",&t);

  std::ofstream ofile(TString(workdir)+"/job/Dat/"+TString(fileName) + ".dat");
  ofile<<"# x(mm)   xp(mrad)   y(mm)   yp(mrad)  t(mm/c)  p(MeV/c)"<<std::endl;
  
  Long64_t nentries = tree->GetEntries();

  for(Long64_t i = 0; i<nentries; i++) {

    pdgId = 0;
    x = 0;
    y = 0;
    px = 0;
    py = 0;
    pz = 0;
    t = 0;

    tree->GetEntry(i);

    if(pdgId != -11) continue;

    if(TMath::IsNaN(px)) continue;
    if(TMath::IsNaN(py)) continue;
    if(TMath::IsNaN(pz)) continue;
    if(pz==0) continue;

    Double_t xp = (px/pz) * 1e3; // [rad] -> [mrad]
    Double_t yp = (py/pz) * 1e3;

    Double_t p  = sqrt(px*px+py*py+pz*pz);  // [MeV/c]

    ofile<< x<<"\t"<<xp<<"\t"<<y<<"\t"<<yp<<"\t"<<t<<"\t"<<p<<std::endl;
  }

  ofile.close();

  fileROOT->Close();
  delete fileROOT;
}
