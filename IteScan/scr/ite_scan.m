#!/usr/bin/env octave

function ite_scan(argv)

  %% Arguments

  if(size(argv)(1)!=4)
    disp("Error:: incorrect parameters!");
    return;
  endif

  suffix    = argv{1}; %% opt. option, e.g. 'trk_XXX_1st'
  
  %% Useful flags

  run_local = str2num(argv{3}); %% 0: submit batch jobs;    1: local run
  auto_job = 0; %% 0: submit all jobs;    1: skip finished jobs

  %% Global variables

  global gg; %% global general parametres
  global gp; %% global opt. parameters

  gg.seed   = argv{2}; %% random seed

  gg.workdir = pwd();
  gg.TargetOutput = "";
  gg.suffix = "";
  gg.fullsuffix = "";

  %% flags for if tracking each subsystem (set all to 1 for opt.)
  gg.track_Target = 1; 
  gg.track_AMD    = 1;
  gg.track_TW     = 1;
  gg.track_IL     = 1;
  if (gg.track_TW) 
    gg.track_AMD = 1;  %% gp.amd_L needs to be recalculated
  endif
  Flag = [ gg.track_Target, gg.track_AMD, gg.track_TW, gg.track_IL ];

  gg.xtal_width = 100; %% mm
  gg.amor_width = 100; %% mm
  gg.tree_option = argv{4}; %% "all", "primary", "photon_emit", "xtal_leave", "amor_arrive", "amor_leave", "amd_arrive","amd_leave"
  if( strncmp(suffix, "trk_",4) ) gg.tree_option = "all"; endif

  gg.opt_running = 0; %% if doing optimisation, set automatically
  if( strncmp(suffix, "scan_",5) ) gg.opt_running = 1; endif
  gg.ref_t    	= 0; %% [mm/c], initialise reference time for IL
  gg.ref_e 	= 0; %% [MeV], initialise reference energy for IL
  gg.e_def 	= 0; %% [MeV], energy deficit for IL to reach designed energy
  gg.energy_IL 	= 2860; %% [MeV], designed energy after IL
  gg.delta_t 	= 19.8; %% IL time window [mm/c], sigmaZ = 3.3 mm

  gg.Ne 	= 1e4; %% number of simulated electrons 
  gg.fb        	= 50; %% frequency of bunches [Hz]
  gg.f_rf 	= 1.9986e9; %% frequency of RF field for TW and IL [Hz]
  gg.sigmaEp = 1.2e-2; %% PDR momentum acceptance

  gg.amd_linf_act = 1; %% if use AMD linear fringe field
  gg.amd_linf_K = 0.5; %% K for AMD linear fringe field
  gg.amd_linf_B0ZPos = 5; %% B0 position for AMD linear fringe field [mm]

  %% AMD options: AnalyticFCLike, LinearFC, NonLinearFC
  %% reset in parameter file in par/
  gg.amd_option = '';

  gg.amd_scaling = 1;

  gg.sol_bz = 0.5; %% NC solenoid field [T]
  gg.tw_dr1 = 0.20; %% TW distance between RF1 and RF2 [m]
  gg.tw_dr2 = 0.20; %% TW distances for other RFs [m]

  %% Initialise other general parameters
  gg.dPxy = 0;
  gg.mesh_dxy 	= 0.5; %% [mm], mesh vloume size
  gg.mesh_dz  	= 0.5; %% [mm]
  gg.size_mesh_xy  = 0; %% set automatically
  gg.size_mesh_z   = 0;
  gg.nbins_mesh_xy = 0;
  gg.nbins_mesh_z  = 0;
  gg.size_mesh_xy_xtal  = 0; %% set automatically
  gg.size_mesh_z_xtal   = 0;
  gg.nbins_mesh_xy_xtal = 0;
  gg.nbins_mesh_z_xtal  = 0;
  
  %% Initialise optimisation parameters
  gp.energy     = 0;
  gp.sigmaXY    = 0;
  gp.emittance  = 0;
  gp.xtal_thick = 0;
  gp.distance   = 0;
  gp.mag_field  = 0;
  gp.amor_thick = 0;
  gp.amd_fgap   = 0;
  gp.B0         = 0;
  gp.amd_L      = 0;
  gp.amd_R1     = 0;
  gp.tw_fgap    = 0;
  gp.phi_dec    = 0;
  gp.phi_acc    = 0;
  gp.grad_dec   = 0;
  gp.grad_acc   = 0;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  %% General parameters
  if( strfind(suffix, "CLIC380GeV") )
    gg.dE     	= 1e-3; %% energy spread [-]
    gg.sigmaT	= 1; 	%% bunch length [mm/c]
    gg.Nb 	= 352; 	%% number of bunches per pulse (380 GeV: 352; 1.5 TeV: 312; 3 TeV: 312)
    gg.Np 	= 5.2e9*1.2; %% positron bunch population (380 GeV: 5.2e9*1.2; 1.5 & 3 TeV: 3.7e9*1.2)
  elseif( strfind(suffix, "CLIC3TeV") )
    gg.dE     	= 1e-3; %% energy spread [-]
    gg.sigmaT	= 1; 	%% bunch length [mm/c]
    gg.Nb 	= 312; 	%% number of bunches per pulse (380 GeV: 352; 1.5 TeV: 312; 3 TeV: 312)
    gg.Np 	= 3.7e9*1.2; %% positron bunch population (380 GeV: 5.2e9*1.2; 1.5 & 3 TeV: 3.7e9*1.2)
  endif
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
  %% If step = 0, no scanning, use starting point value instead
  STEP = RMIN = RMAX = zeros(1,16); %% initialise

  %% For test
  %RMIN(1) = 5;
  %RMAX(1) = 5;
  %STEP(1) = 1;

  %% Default for the 1st / final scan
  %% Units:  GeV,  mm, mm*mrad,  mm,    m,   T,  mm,    mm,   T, cm, mm,    mm, deg, deg, MV/m, MV/m
  RMIN =   [ 0.5, 0.6,      50,   0,    0,   0,   5,     0, 0.5, 10,  3,     0,   0,   0,    6,    6 ];
  RMAX =   [ 9.5, 6.5,     100,   3,  2.5,   2,  25,     6,   8, 30, 10,   100, 360, 360,   26,   26 ];
  STEP =   [ 0.5, 0.1,      10, 0.1, 0.25, 0.2,   1,   0.5, 0.5,  1,  1,    10,   5,   5,    1,    1 ];

  %% Optimisation parameters (starting point for scan)
  if (0)
  else
    if( strncmp(suffix, "trk_",4) )
      par_file = regexprep(suffix,"trk_","","once");
    elseif( strncmp(suffix, "scan_",5) )
      par_file = regexprep(suffix,"scan_","","once");
    else
      disp("ERROR:: Wrong option!");
    endif
    try
      source([ gg.workdir "/par/" par_file ".m" ]);
    catch
      disp("ERROR:: Parameter file not found! Initial parameter values will be used.");
    end_try_catch
  endif
  
  P = [ gp.energy, gp.sigmaXY, gp.emittance, gp.xtal_thick, gp.distance, gp.mag_field, gp.amor_thick, ...
        gp.amd_fgap, gp.B0, gp.amd_L, gp.amd_R1, ...
	gp.tw_fgap, gp.phi_dec, gp.phi_acc, gp.grad_dec, gp.grad_acc ];

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %% Set all flags to 1 for optimisation
  %if( gg.opt_running && !(gg.track_Target&&gg.track_AMD&&gg.track_TW&&gg.track_IL) )
  if( gg.opt_running )
    disp("INFO:: Forcing all flags to be 1 for optimisation.");
    gg.track_Target = gg.track_AMD = gg.track_TW = gg.track_IL = 1; 
  endif
  
  %% Set path
  addpath("scr");
  
  %% Tracking
  if (strncmp(suffix, "trk_",4))
    P_track = P;
    filename_arg = ["job/Arguments/" suffix "_0_0.dat"];
    save("-text",filename_arg,"P_track","Flag","gp","gg");
    if (run_local==1)
      arguments = [ suffix " " gg.workdir " 0 0" ];
      system(["./scr/job.sh " arguments]);
    else
      arguments = [ suffix ", " gg.workdir ", 0, 0" ];
      system("rm -f job/Condor/job_list.txt");
      system("touch job/Condor/job_list.txt");
      system(["echo \'" arguments "\' >> job/Condor/job_list.txt"]);
      system(["cat scr/submit.sub"]);
      system(["cat job/Condor/job_list.txt"]);
      system(["cat job/Condor/job_list.txt|wc -l"]);
      system(["condor_submit scr/submit.sub"]);
    endif
  
  %% Scanning
  elseif ( strncmp(suffix, "scan_",5) )
    n_par = size(P)(2);
    system("rm -f job/Condor/job_list.txt");
    system("touch job/Condor/job_list.txt");
    for i = 1 : n_par
      P_track = P;
      if( STEP(i) == 0 )
        continue;
      else
        n_step = round( (RMAX(i) - RMIN(i)) / STEP(i) );
      endif
      for j = 1 : (n_step+1)
        P_track(i) = RMIN(i) + (j-1) * STEP(i);
        Flag = [ gg.track_Target, gg.track_AMD, gg.track_TW, gg.track_IL ];
        i_str = num2str(i);
        j_str = num2str(j);
        if (auto_job) %% skip finished jobs
  	  try
            nP = load(["job/Results/" suffix "_" i_str "_" j_str ".dat"]).nPositrons;
  	  catch
  	    nP = -99;
  	  end_try_catch
  	  if (nP!=-99 && nP!=-1) continue; endif;
        endif
        filename_arg = ["job/Arguments/" suffix "_" i_str "_" j_str ".dat"];
        save("-text",filename_arg,"P_track","Flag","gp","gg");
        if (run_local==1)
          arguments = [ suffix " " gg.workdir " " i_str " " j_str ];
          system(["./scr/job.sh " arguments]);
        else
          arguments = [ suffix ", " gg.workdir ", " i_str ", " j_str ];
          system(["echo \'" arguments "\' >> job/Condor/job_list.txt"]);
        endif
      endfor
    endfor
    if (run_local==0)
      system(["cat scr/submit.sub"]);
      system(["cat job/Condor/job_list.txt"]);
      system(["cat job/Condor/job_list.txt|wc -l"]);
      system(["condor_submit scr/submit.sub"]);
    endif
  endif %% end of tracking and scanning
endfunction

ite_scan(argv);
