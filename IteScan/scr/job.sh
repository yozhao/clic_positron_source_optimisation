#!/bin/bash

suffix=$1
workdir=$2
i_str=$3
j_str=$4

if [[ $(hostname) != "lxclicbpk20" ]]; then
  source /cvmfs/clicbp.cern.ch/x86_64-centos7-gcc8-opt/setup.sh
fi

octave-cli -q ${workdir}/scr/job.m $suffix $workdir $i_str $j_str
