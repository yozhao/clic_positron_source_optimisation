function prepare_G4(out_filename)

  fout = fopen(out_filename,"w");

  global gg;
  global gp;

  fprintf(fout, "/xtal/setSize %f %f %f mm \n\n",gg.xtal_width,gg.xtal_width,gp.xtal_thick);

  fprintf(fout, "/amorphous/setDistance 0 0 %f m \n",gp.distance);
  fprintf(fout, "/amorphous/setSize %f %f %f mm \n",gg.amor_width,gg.amor_width,gp.amor_thick);

  fprintf(fout, "/dipole/setMagField 0 %f 0 tesla \n\n",gp.mag_field);

  fprintf(fout, "/amd/setFrontGapSize 100 100 0 mm \n\n");

  fprintf(fout, "/amd/setLength 0 mm \n\n");

  fprintf(fout, "/amd/LinFringe/isActive %i \n",gg.amd_linf_act);
  fprintf(fout, "/amd/LinFringe/setB0 %f tesla \n",gp.B0);
  fprintf(fout, "/amd/LinFringe/setK_T_MM %f \n",gg.amd_linf_K);
  fprintf(fout, "/amd/LinFringe/setB0ZPos %f mm \n",gg.amd_linf_B0ZPos);
  fprintf(fout, "/amd/LinFringe/setTargZPos %f mm \n\n",-gp.amd_fgap);

  fprintf(fout, "/run/initialize \n\n");

  fprintf(fout, "/gun/primaryEnergy %f MeV \n",   gp.energy*1e3);
  fprintf(fout, "/gun/sigmaUserE    %f MeV \n",   gg.sigmaE);
  fprintf(fout, "/gun/sigmaUserXY   %f mm  \n",   gp.sigmaXY);
  fprintf(fout, "/gun/sigmaUserPxy  %f MeV \n",   gg.sigmaPxy);
  fprintf(fout, "/gun/sigmaUserT    %f     \n\n", gg.sigmaT);     %% [mm/c]

  fprintf(fout, "/output/filename %s/job/Root/%s.root \n\n",gg.workdir,gg.TargetOutput);

  fprintf(fout, "/tracking/verbose 0 \n\n");

  if( !gg.opt_running && gp.xtal_thick!=0)
    fprintf(fout, "/score/create/boxMesh Mesh_xtal \n");
    fprintf(fout, "/score/mesh/boxSize %f %f %f mm \n", gg.size_mesh_xy_xtal/2.0, gg.size_mesh_xy_xtal/2.0, gg.size_mesh_z_xtal/2.0);
    fprintf(fout, "/score/mesh/nBin %i %i %i \n", gg.nbins_mesh_xy_xtal, gg.nbins_mesh_xy_xtal, gg.nbins_mesh_z_xtal);
    fprintf(fout, "/score/mesh/translate/xyz 0 0 %f m \n", (gp.xtal_thick/2.0)/1e3);
    fprintf(fout, "/score/quantity/energyDeposit eDep_xtal \n");
    fprintf(fout, "/score/close \n\n");
  endif

  fprintf(fout, "/score/create/boxMesh Mesh_amorph \n");
  fprintf(fout, "/score/mesh/boxSize %f %f %f mm \n", gg.size_mesh_xy/2.0, gg.size_mesh_xy/2.0, gg.size_mesh_z/2.0);
  fprintf(fout, "/score/mesh/nBin %i %i %i \n", gg.nbins_mesh_xy, gg.nbins_mesh_xy, gg.nbins_mesh_z);
  fprintf(fout, "/score/mesh/translate/xyz 0 0 %f m \n", (gp.xtal_thick + gp.distance*1000 + gp.amor_thick/2.0)/1e3);
  fprintf(fout, "/score/quantity/energyDeposit eDep_amorph \n");
  fprintf(fout, "/score/close \n\n");

  fprintf(fout, "/run/beamOn %i \n\n", gg.Ne);

  if( !gg.opt_running && gp.xtal_thick!=0)
    fprintf(fout, "/score/dumpQuantityToFile Mesh_xtal eDep_xtal %s/job/Dat/%s_xtal.dat \n", gg.workdir,gg.TargetOutput);
  endif
  fprintf(fout, "/score/dumpQuantityToFile Mesh_amorph eDep_amorph %s/job/Dat/%s_amorph.dat \n", gg.workdir,gg.TargetOutput);

  fclose(fout);

endfunction
