
function job(argv)

  suffix  = argv{1};
  workdir = argv{2};
  i_str   = argv{3};
  j_str   = argv{4};

  addpath([workdir "/scr"]);

  %% Retrieve arguments from Arguments/
  filename_arg = [ workdir "/job/Arguments/" suffix "_" i_str "_" j_str ".dat" ];
  P_track  = load(filename_arg).P_track;
  Flag     = load(filename_arg).Flag;

  %% Declare global variables
  global gg = load(filename_arg).gg;
  global gp = load(filename_arg).gp;

  %% Log file name
  filename_log = [ workdir "/job/Log/" suffix "_" i_str "_" j_str ".log" ];

  system(["rm -f " filename_log]);
  diary (filename_log);

  %% Call track.m
  [nPositrons, pedd, power_deposit, power_beam] = track(P_track, Flag, suffix, i_str, j_str);

  %% Calculate energy spread at the end of TW
  if(gg.track_TW || gg.track_IL) 
    filename_TW = [workdir "/job/Dat/" gg.TWOutput ".dat"];
    %printf("INFO:: Calculating e_spread for: %s\n", filename_TW);
    A_TW = load(filename_TW).A_TW;
    M_E = A_TW(:,6) > 50 & A_TW(:,6) < 350;
    e_mean_TW = mean(A_TW(M_E,6));
    e_spread_TW = std(A_TW(M_E,6)) / mean(A_TW(M_E,6));
    printf("INFO:: Check at TW exit (50 MeV < E < 350 MeV):\n");
    printf("       e_mean: %f MeV\n", e_mean_TW);
    printf("       e_spread: %.1f%%\n", e_spread_TW*100);
  endif

  %% Calculate mean energy at the end of IL
  %if(gg.track_IL && 1) 
  %  filename_IL = [workdir "/job/Dat/" gg.ILOutput ".dat"];
  %  A_IL = load(filename_IL).A_IL;
  %  e_mean_IL = mean(A_IL(:,6));
  %  e_spread_IL = std(A_IL(:,6)) / mean(A_IL(:,6));
  %  printf("INFO:: check at IL exit:\n");
  %  printf("       e_mean: %f MeV\n", e_mean_IL);
  %  printf("       e_spread: %.1f%%\n", e_spread_IL*100);
  %endif

  diary off;

  %% Save into Results/
  yield = nPositrons * 1.0 / gg.Ne;
  filename = [workdir "/job/Results/"   suffix "_" i_str "_" j_str ".dat"];
  save("-text",filename,"nPositrons","yield","pedd","power_deposit","power_beam","e_mean_TW","e_spread_TW","P_track","Flag","gp","gg");

  %% Delete unmerged root files
  if(gg.track_Target) 
    fullSuffix = [suffix "_" i_str "_" j_str];
    TargetOutput = ["Target_" fullSuffix];
    system(["rm -f " workdir "/job/Root/" TargetOutput "_t*.root"]);
  endif

endfunction

job(argv);

