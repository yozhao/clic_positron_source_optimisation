function power = calc_power(nPositrons)

  global gg;
  global gp;

  printf("INFO:: Beam power calculation parameters:\n");
  printf("       energy: %f [GeV]\n", gp.energy);
  printf("       Np: %i\n", gg.Np);
  printf("       Nb: %i\n", gg.Nb);
  printf("       fb: %f [Hz]\n", gg.fb);

  %% (in-situ) yield

  yield = nPositrons * 1.0 / gg.Ne;

  %% (in-situ) Beam Power %%

  power = (gp.energy*1e3) * (gg.Np/yield * gg.Nb * gg.fb * 1.6e-19*1e3); %% kW

  printf("INFO:: Given yield = %f, Beam power = %f kW\n", yield,power);

  %% given yield=1
  printf("INFO:: Given yield = 1, Beam power = %f kW\n", (power*yield/1.0) );

endfunction
