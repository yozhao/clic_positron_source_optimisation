
  gg.amd_option = 'LinearFC'; % Linear aperture FC

  gg.amd_linf_act = 0; %% if use AMD linear fringe field

  gp.energy     = 5; %% [GeV]
  gp.sigmaXY    = 2.3;  %% [mm]
  gp.emittance  = 80; %% [mm.mrad]
  gp.xtal_thick   = 0; %% [mm]
  gp.distance     = 0; %% [m]
  gp.mag_field    = 0; %% [T]
  gp.amor_thick = 18; %% [mm]
  gp.amd_fgap = 2; %% [mm]
  gp.B0     = 6; %% [T], scaled peak Bz
  gp.amd_L  = 0; %% [cm]
  gp.amd_R1 = 0; %% [mm]
  gp.tw_fgap = 50; %% [mm]
  gp.phi_dec  = 175; %% degree
  gp.phi_acc  = 175;
  gp.grad_dec = 20; %% MV/m
  gp.grad_acc = 19;

  %% variables not to be scanned in optimisation
  STEP(3) = 0;
  STEP(4:6) = 0;
  STEP(10:11) = 0;
