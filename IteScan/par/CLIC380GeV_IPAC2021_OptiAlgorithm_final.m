
  gg.amd_option = 'AnalyticFCLike'; % FC-like, Linear fringe + Adiabatic

  gp.energy     = 5; %% [GeV]
  gp.sigmaXY    = 2.2;  %% [mm] %% free
  gp.emittance  = 80; %% [mm.mrad]
  gp.xtal_thick   = 0; %% [mm]
  gp.distance     = 0; %% [m]
  gp.mag_field    = 0; %% [T]
  gp.amor_thick = 18; %% [mm] %% free
  gp.amd_fgap = 2; %% [mm]
  gp.B0 = 6; %% [T] %% free
  gp.amd_L = 22; %% [cm] %% free
  gp.amd_R1 = 8; %% [mm] %% free
  gp.tw_fgap = 50; %% [mm]
  gp.phi_dec  = 160; %% degree %% free
  gp.phi_acc  = 155; %% free
  gp.grad_dec = 13; %% MV/m %% free
  gp.grad_acc = 17; %% free

  %% variables not to be scanned in optimisation

  STEP(1) = 0;
  STEP(3:6) = 0;
  STEP(8) = 0;
  STEP(12) = 0;

  %% reset scan step

  STEP(13:14) = 5;
