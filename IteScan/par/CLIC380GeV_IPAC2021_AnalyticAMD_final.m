
  gg.amd_option = 'AnalyticFCLike'; % FC-like, Linear fringe + Adiabatic

  gp.energy     = 5; %% [GeV]
  gp.sigmaXY    = 2.2;  %% [mm]
  gp.emittance  = 80; %% [mm.mrad]
  gp.xtal_thick   = 0; %% [mm]
  gp.distance     = 0; %% [m]
  gp.mag_field    = 0; %% [T]
  gp.amor_thick = 18; %% [mm]
  gp.amd_fgap = 2; %% [mm]
  gp.B0 = 6; %% [T]
  gp.amd_L = 22; %% [cm]
  gp.amd_R1 = 8; %% [mm]
  gp.tw_fgap = 50; %% [mm]
  gp.phi_dec  = 160; %% degree
  gp.phi_acc  = 155;
  gp.grad_dec = 13; %% MV/m
  gp.grad_acc = 17;

  STEP(4:6) = 0; %% variables not to be scanned in optimisation
