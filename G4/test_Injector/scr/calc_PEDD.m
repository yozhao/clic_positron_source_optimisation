function pedd = calc_PEDD()

  option = argv(){1};

  %xtal_file = ["output/Target_trk_" option "_xtal.dat"];
  %amor_file = ["output/Target_trk_" option "_amor.dat"];
  xtal_file = ["output/Target_trk_output_xtal.dat"];
  amor_file = ["output/Target_trk_output_amor.dat"];

  try
    A_xtal = load(xtal_file);
  catch
    A_xtal = [0 0 0 0];
  end_try_catch

  A_amor = load(amor_file);

  peak_energy_xtal = max(A_xtal(:,4)) * 1e-3; %% GeV
  peak_energy_amor = max(A_amor(:,4)) * 1e-3; %% GeV

  total_energy_xtal = sum(A_xtal(:,4)) * 1e-3; %% GeV
  total_energy_amor = sum(A_amor(:,4)) * 1e-3; %% GeV

  Ne_s = 1e4; %% number of simulated e-

  targ_width = 100; %% mm

  if (strcmp(option,"test_FCCee_Hybrid_NoFringe"))
    xtal_thick = 1.4; %% mm
    amor_thick = 12; %% mm
    Nb_e = 2; %% number of e- bunches per pulse
    Ne_b = 4.2e10; %% e- bunch charge
    f_rep = 200; %% Hz, pulse frequency
  elseif (strcmp(option,"test_FCCee_Conventional_NoFringe"))
    xtal_thick = 0; %% mm
    amor_thick = 16; %% mm
    Nb_e = 2; %% number of e- bunches per pulse
    Ne_b = 4.2e10; %% e- bunch charge
    f_rep = 200; %% Hz, pulse frequency
  elseif (strcmp(option,"test_CLIC_3TeV_Hybrid_NoFringe"))
    xtal_thick = 1; %% mm
    amor_thick = 15; %% mm
    Nb_e = 312; %% number of e- bunches per pulse
    Np_b = 3.7e9*1.2; %% target e+ bunch charge
    yield = 2.0; %% given yield
    Ne_b = Np_b / yield; %% e- bunch charge
    f_rep = 50; %% Hz, pulse frequency
    printf("INFO:: Given yield = %.1f \n", yield);
  elseif (strncmp(option,"test_CLIC_3TeV_Conventional_",28))
    xtal_thick = 0; %% mm
    amor_thick = 16; %% mm
    Nb_e = 312; %% number of e- bunches per pulse
    Np_b = 3.7e9*1.2; %% target e+ bunch charge
    yield = 2.0; %% given yield
    Ne_b = Np_b / yield; %% e- bunch charge
    f_rep = 50; %% Hz, pulse frequency
    printf("INFO:: Given yield = %.1f \n", yield);
  endif

  nbins_xy = round(targ_width*2 + 1);
  delta_xy = (targ_width + 0.5) / nbins_xy;
  delta_x = delta_xy;
  delta_y = delta_xy;

  nbins_z = round(xtal_thick*2 + 1);
  delta_z = (xtal_thick + 0.5) / nbins_z;
  volume_cell_xtal = delta_x * delta_y * delta_z * 1.e-3; 	%% cm^3

  nbins_z = round(amor_thick*2 + 1);
  delta_z = (amor_thick + 0.5) / nbins_z;
  volume_cell_amor = delta_x * delta_y * delta_z * 1.e-3; 	%% cm^3

  density = 19.25; 	%% g/cm^3
  factor_GeV_to_J = 1.60218e-10;
  factor_GeV_to_kW = factor_GeV_to_J*1.0e-3; %% per second

  pedd_xtal = peak_energy_xtal * (Nb_e*Ne_b/Ne_s) / (volume_cell_xtal*density) * factor_GeV_to_J;
  pedd_amor = peak_energy_amor * (Nb_e*Ne_b/Ne_s) / (volume_cell_amor*density) * factor_GeV_to_J;

  power_xtal = total_energy_xtal * (f_rep*Nb_e*Ne_b/Ne_s) * factor_GeV_to_kW;
  power_amor = total_energy_amor * (f_rep*Nb_e*Ne_b/Ne_s) * factor_GeV_to_kW;

  printf("INFO:: Target PEDD (xtal.): %f [J/g]\n", pedd_xtal);
  printf("       max(E_deposited): %f [GeV]\n", peak_energy_xtal);
  printf("       Deposited energy: %f [GeV]\n", total_energy_xtal);
  printf("       Deposited power: %f [kW]\n", power_xtal);

  printf("INFO:: Target PEDD (amor.): %f [J/g]\n", pedd_amor);
  printf("       max(E_deposited): %f [GeV]\n", peak_energy_amor);
  printf("       Deposited energy: %f [GeV]\n", total_energy_amor);
  printf("       Deposited power: %f [kW]\n", power_amor);

endfunction
calc_PEDD();
