void calc_NP(const char *fullfilename = "output/Target_trk_output.root"){

  TFile* f = new TFile(fullfilename);
  
  double n;
  
  TTree* xtal_leave = (TTree*) f->Get("xtal_leave");
  if(xtal_leave){
    cout<<"INFO:: at xtal. exit:"<<endl;
    n = xtal_leave->GetEntries("pdgId==11");
    cout<<"  N_e-: "<<n<<endl;
    n = xtal_leave->GetEntries("pdgId==22");
    cout<<"  N_gamma: "<<n<<endl;
    n = xtal_leave->GetEntries("pdgId==-11");
    cout<<"  N_e+: "<<n<<endl;
  }
  
  TTree* amor_arrive = (TTree*) f->Get("amor_arrive");
  if(amor_arrive){
    cout<<"INFO:: at amor. entrance:"<<endl;
    n = amor_arrive->GetEntries("pdgId==11");
    cout<<"  N_e-: "<<n<<endl;
    n = amor_arrive->GetEntries("pdgId==22");
    cout<<"  N_gamma: "<<n<<endl;
    n = amor_arrive->GetEntries("pdgId==-11");
    cout<<"  N_e+: "<<n<<endl;
  }
  
  TTree* amor_leave = (TTree*) f->Get("amor_leave");
  if(amor_leave){
    cout<<"INFO:: at amor. exit:"<<endl;
    n = amor_leave->GetEntries("pdgId==11");
    cout<<"  N_e-: "<<n<<endl;
    n = amor_leave->GetEntries("pdgId==22");
    cout<<"  N_gamma: "<<n<<endl;
    n = amor_leave->GetEntries("pdgId==-11");
    cout<<"  N_e+: "<<n<<endl;
  }
  
  f->Close();
}
