#!/bin/bash

mkdir -p output Results

## Options
suffix=CLIC3TeV_Nov2020_HugoAMD_t1

config_file=config/${suffix}.mac
output_suffix=Target_trk_output

ncore=$(nproc --all)

## options: "all", "primary", "photon_emit", "xtal_leave", "amor_arrive", "amor_leave", "amd_arrive","amd_leave"
tree_option="all"

seed=1
../Injector_build/injector $config_file ${ncore} $tree_option $seed |& tee output/log.injector

## Merge outputs
echo "Merging root files ..."
if [[ $ncore -gt 1 ]];then
  hadd -f output/${output_suffix}.root output/${output_suffix}_t*.root
  rm -f output/${output_suffix}_t*.root
elif [[ $ncore -eq 1 ]];then
  mv output/${output_suffix}_t0.root output/${output_suffix}.root
fi

## Calculate NP
root -l -b -q scr/calc_NP.C\(\"output/${output_suffix}.root\"\) |& tee output/log.calc_NP

## Generate dat file
root -l -b -q scr/convert_ROOT_To_RFT.C\(\"output/${output_suffix}.root\",\"amor_leave\"\)
root -l -b -q scr/convert_ROOT_To_RFT.C\(\"output/${output_suffix}.root\",\"amd_leave\"\)

## Calculate PEDD
#octave-cli -q scr/calc_PEDD.m ${suffix} |& tee output/log.calc_PEDD_${suffix}

## Save
mkdir -pv Results/${suffix}
cp -f output/* Results/${suffix}/
