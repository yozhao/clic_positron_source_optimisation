
------ How to run ------

0) Setup

  > source setup.sh

  Note: modification might be needed depending on your computing environment

1) Compile Geant4

  > cd G4/
  > ./compile.sh
  > cd ../

  Note: modification might be needed depending on your Geant4 installation

2) Run the tracking or optimisation (analytic injector linac)

  > cd IteScan/
  > ./run.sh
  > cd ../

  Note: results are saved in job/Results/, job/Dat/, job/Log/, etc.

3) Injector linac simulation

  > cd Placet/
  > cd ../

  Note: Placet should be available
------------------------

