
dir_1=../clic_positron_source_optimisation
dir_2=../git_clic_positron_source_optimisation

allfiles=($(du -ah $dir_2))
for filename in ${allfiles[*]}
do
  [[ ! -f $filename ]] && continue
  filename=${filename/${dir_2}\//}
  [[ ! -f $dir_1/$filename ]] && continue
  if [[ $(diff -q $dir_1/$filename $dir_2/$filename) ]];then
    #echo "--- $filename changed: ---"
    #diff $dir_1/$filename $dir_2/$filename
    echo "--- $filename updated ---"
    cp $dir_1/$filename $dir_2/$filename
  fi
done

