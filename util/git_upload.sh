#!/bin/bash

#comments="Big update"
comments="Small update"

if [ $# -ne 0 ];then
  comments=$@
fi

## discard old commits
#git reset --soft

git add -A .
git commit -m "${comments}"

git push --force origin master

